#!/usr/bin/env python3

import socket
from _thread import *

PORT = 18000


def handle_job(conn, addr):
    # Function to handle job that copies file from Master to Worker

    msg = "\nMessage from Server: Hi Client " + addr[0] + "!\n"
    conn.send(msg.encode())

    file = open("output.txt", "wb")
    print("\nCopied file name will be output.txt at server side")

    RecvData = conn.recv(1024)
    print("Start copying file from Client...")

    while RecvData:
        file.write(RecvData)
        RecvData = conn.recv(1024)

    file.close()
    print("File has been copied successfully")
    conn.close()
    print("Server closed the connection\n\n----------")


s = socket.socket()
s.bind(('0.0.0.0', PORT))
s.listen(10)
while True:
    conn, addr = s.accept()
    start_new_thread(handle_job, (conn, addr))
