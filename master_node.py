import socket
import csv
from _thread import *

active = [('3.86.240.28', 17000), ('34.238.249.120', 17001), ('3.85.129.217', 17002)]
CSV_FILE = 'worker_status.csv'


def threaded_client(worker_address, file):
    # Function to send job request to Worker
    try:
        ClientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        ClientSocket.connect(worker_address)
        set_worker_status(worker_address[0], 'BUSY')
        print("\n[WORKER " + str(worker_address[0]) + " | PORT: " + str(worker_address[1]) + "] = Job received")
        SendData = file.read(1024)

        while SendData:
            print(ClientSocket.recv(1024).decode("utf-8"))
            print("\n[WORKER " + str(worker_address[0]) + " | PORT: " + str(worker_address[1]) + "] = Job is running")
            ClientSocket.send(SendData)
            SendData = file.read(1024)

        print("\n[WORKER " + str(worker_address[0]) + " | PORT: " + str(worker_address[1]) + "] = Job is finished")
        ClientSocket.close()
        active.append(worker_address)
        set_worker_status(worker_address[0], 'ACTIVE')

    except:
        print("\n[WORKER " + str(worker_address[0]) + " | PORT: " + str(worker_address[1]) + "] = Job failed")


def monitor_worker():
    # Function to print out all Worker's status
    with open(CSV_FILE) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            print(row[0] + " | " + row[1] + " | " + row[2])


def set_worker_status(ip_address, status):
    # Function to set a Worker's status
    with open(CSV_FILE) as csv_file:
        csv_reader = csv.DictReader(csv_file)
        updated_data = []
        for row in csv_reader:
            if row['IP Address'] == ip_address:
                row['Status'] = status
            data = (row['IP Address'], row['Port Number'], row['Status'])
            updated_data.append(data)
        header = ('IP Address', "Port Number", "Status")
        writer(header, updated_data)


def writer(header, data):
    # Function to write to CSV_FILE
    with open(CSV_FILE, "w", newline="") as csv_file:
        csv_writer = csv.writer(csv_file)
        csv_writer.writerow(header)
        for x in data:
            csv_writer.writerow(x)


def start_master():
    try:
        header = ('IP Address', "Port Number", "Status")
        data = [
            ('3.86.240.28', 17000, 'ACTIVE'),
            ('34.238.249.120', 17001, 'ACTIVE'),
            ('3.85.129.217', 17002, 'ACTIVE')
        ]
        writer(header, data)

        while True:
            input_user = input('Input command: ')
            if input_user == 'SEND':
                if len(active) >= 1:
                    fileName = input("File name (.txt) : ")
                    file = open(fileName, "rb")
                    target = active[0]
                    active.remove(target)
                    start_new_thread(threaded_client, (target, file))
                else:
                    print("Sorry, there is no active Worker right now. Please try again later :)")
            elif input_user == 'MONITOR':
                print("[ MONITOR ]")
                monitor_worker()
            elif input_user == 'END_JOBS':
                print("All job are stopped")
                break
            else:
                print("Unknown input")
                option = input("Want to end connection now? (YES/NO) ")
                if option == 'YES':
                    break

    except Exception as e:
        print("Failed to execute")
        print(e)


if __name__ == '__main__':
    start_master()
