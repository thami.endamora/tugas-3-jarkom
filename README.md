# Tugas 3 Jarkom : *Socket Programming*

Sistem yang kami kembangkan merupakan implementasi *socket programming* yang melakukan *job management*. Terdapat satu buah Master node yang menerima *input* berupa *command* dari pengguna dan mengirimkan *request* kepada Worker node. Selain itu, terdapat tiga buah Worker node berupa virtual server EC2 yang akan melaksanakan *job* dari Master.

## Developer Team
Kelompok G1
1. 1806141170 - [Dinda Inas Putri](https://gitlab.com/dindainas)
2. 1806141220 - [Ilma Ainur Rohma](https://gitlab.com/ilmarohma)
3. 1806141460 - [Thami Endamora](https://gitlab.com/thami.endamora)

## Development Tools
- Instance server dengan service Amazon EC2 (Amazon Elastic Compute Cloud) 
- Python 3 dengan menggunakan library socket

## Detail Sistem
1. [Master Node](https://gitlab.com/thami.endamora/tugas-3-jarkom/-/blob/master/master_node.py) dijalankan di local komputer sebagai node yang akan mengirimkan data ke server. IP dan port dapat disesuaikan dengan worker mana yang ingin dikirimkan datanya. [Master Node](https://gitlab.com/thami.endamora/tugas-3-jarkom/-/blob/master/master_node.py) juga melakukan pencatatan IP dan Port mana yang aktif, dan yang sedang menjalankan job. 
2. [Worker Node](https://gitlab.com/thami.endamora/tugas-3-jarkom/-/blob/master/worker_node.py) dijalankan di dalam server EC2 sebagai node yang akan meng-copy data dari master. Port pada EC2 dapat disesuaikan dengan port yang memang masih tersedia.

---

### Catatan :
1. Sumber referensi kode: 
- [linuxhint](https://linuxhint.com/python_socket_file_transfer_send/) 
- [realpython](https://realpython.com/python-sockets/)
- [free-code-camp](https://medium.com/free-code-camp/how-to-create-read-update-and-search-through-excel-files-using-python-c70680d811d4)
- [codezup](https://codezup.com/socket-server-with-multiple-clients-model-multithreading-python/)
2. Program ini kami kerjakan bersama-sama secara syncronous menggunakan aplikasi video conference. 
